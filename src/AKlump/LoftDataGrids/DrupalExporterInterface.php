<?php
namespace AKlump\LoftDataGrids;

interface DrupalExporterInterface {

    /**
     * Return a renderable build array
     *
     * @param null $page_id
     *
     * @return array
     */
    public function build($page_id = null);
}
