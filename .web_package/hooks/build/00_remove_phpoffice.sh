#!/usr/bin/env bash

! [ -d vendor/phpoffice ] || rm -r vendor/phpoffice || exit 1
! [ -f vendor/aklump/loft_data_grids/src/XLSXExporter.php ] || rm vendor/aklump/loft_data_grids/src/XLSXExporter.php || exit 1
echo "phpoffice and XLSXExporter removed from codebase for security reasons."
