Summary

   A module wrapper integrating [1]Loft Data Grids with Drupal.

Installation

    1. Download and unzip this module into your modules directory.
    2. Now go to Administer > Site Building > Modules and enable this
       module.

   If you happen to have the [2]PHPExcel Drupal module installed, be aware
   that this module ignores it and uses it's own dependency code for
   PHPExcel contained in the vendor directory of this module.

  User Permissions: UI Only

   Drupal permissions are provided to limit exporter visibility in UI
   functions only. The distinction is that any function in this module
   that provides UI elements (option list, etc) will respect these
   permissions, however api functions will not.

   These permissions can be used globally to remove certain exporters from
   the UI for any dependent module that uses this module's UI functions.

Upgrade to 2.x

   We have now included the dependencies in the distribution of this
   module, rather than requiring that [3]Composer be installed on your
   system to install this module. This decision was made in an effort to
   make this a more "turn-key" solution for users not familiar yet with
   [4]Composer.

   The 2.x branch no longer uses the [5]Libraries API.

  Required

    1. Update the module to the 2.x version.
    2. Delete the folder libraries/loft_data_grids and all it's contents.

  Optional

    1. You may disable [6]Libraries API if it is not required by any other
       module. If unsure, to be safe just leave it alone.
    2. Remove all instances of library_load('loft_data_grids') from your
       code, as it is not longer relevant.

Code Example

   To use any of the classes in [7]Loft Data Grids in your own module, do
   something like this:
<?php
$data = loft_data_grids_export_data();
$data->add('first', 'Aaron');
$data->add('last', 'Klump');
$output = loft_data_grids_exporter($data, 'CSVExporter')->export();
?>

   Refer to the library for more info. It also contains Doxygene docs.

Contact

     * In the Loft Studios
     * Aaron Klump - Developer
     * PO Box 29294 Bellingham, WA 98228-1294
     * aim: theloft101
     * skype: intheloftstudios
     * d.o: aklump
     * [8]http://www.InTheLoftStudios.com

References

   1. https://github.com/aklump/loft_data_grids
   2. https://drupal.org/project/phpexcel
   3. http://getcomposer.org/
   4. http://getcomposer.org/
   5. https://drupal.org/project/libraries
   6. https://drupal.org/project/libraries
   7. https://github.com/aklump/loft_data_grids
   8. http://www.InTheLoftStudios.com/
